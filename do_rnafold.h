#include <ViennaRNA/data_structures.h>
#include <ViennaRNA/eval.h>
#include <ViennaRNA/fold.h>
#include <ViennaRNA/params.h>
#include <ViennaRNA/part_func.h>
#include <ViennaRNA/utils.h>
#include <stdlib.h>
#include <string.h>

#ifndef _DO_RNA_LIB_
#define _DO_RNA_LIB_

#ifdef __cplusplus
extern "C" {
#endif
typedef struct result_rnafold {
  char *structure;
  float fe;
} res_rnafold;

extern void do_rnafold(char const *sequence, char const *constraints,
                   res_rnafold *fold);
extern void do_rnafold_nc(char const *sequence, res_rnafold *fold );

#ifdef __cplusplus
}
#endif

#endif
