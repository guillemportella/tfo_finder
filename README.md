
# Predict polypurine triplexes (T,C)-motifs and (U,C)-motifs

### Searching putative triplex binding sites on DNA for the formation of RNA-DNA<sub>2</sub> triplex.

`tfo_finder` is a tool that finds triplex forming oligonucleotieds (TFOs) in a given (set of) sequence(s). **Only works for polypurine tracks**, resulting in parallel triplexes. It's not a generic tool for all kinds of triplexes. Because of the nature of these triplexes, it is very much `pH` dependent, be careful of which `pH` you use. 

There are a couple of modes for this program, mainly.

1. **Single stranded** : gives back the sequences of the binding partner of the TFOs it finds. It gives back both the sense and antisense. Works for DNA and RNA sequences as input, but using always the same functional model. You can also request using RNAfold to fold the single stranded, this only makes sense for RNA. Futhermore, if you suply constraints in the RNAfold format (modified fastq) these can also be used. Only TFOs found in an unpaired region will be reported.

2. **Double stranded** : gives back the TFO sequences that would bind to the double stranded DNA (e.g. genome). It searches both the fwd and rev strands. The output fasta file containts the coordinates of the motif wrt the + strand in the header. E.g., if it reports |1:10|-| this means that the binding site for the TFO is in the reverse strand, and its 5' starts at 10 and the 3' finishes at 1.

> **Note on using parameters for DNA**: If you want to use the parameters to estimate the stability for triplex DNA, as opposed to RNA-DNA<sub>2</sub>, you would need to make a small change to the source code. Take a look at lines 203 to 205 of `tfo_finder.cpp` on how to do it, it's straightforward. The parameters for triplex DNA from Roberts&Crothers are included, but the option to load them is not coded as an option (I did not need them). Please note that the `-rna` commandline options refers to the expected input, i.e. reading U instead of T, and not the prediction model. To reiterate, we only predict RNA-DNA<sub>2</sub>, you need to make a small change in the code to use DNA<sub>3</sub>. 

#### Usage 

Once compiled, issue 
```bash
./tfo_finder -h
``` 
and you will get a list of the options. I'm afraid I don't have the time for a tutorial, you'd need to explore them yourself. 

The sequences must be introduced using the `fasta` format. It accepts both single entry `fasta`, multiple entry `fasta`, `fastq` files and all the previous files gzip compressed. Check the help for the file extensions recognized.

In broad strokes, use the `-rna` option to indicate that we are reading RNA sequences. Again, this does not mean that we are changing the predictor from DNA<sub>3</sub> to RNA-DNA<sub>2</sub>, the fitted model is for RNA-DNA<sub>2</sub> only. You can choose `-ds` if the sequence you feed in is supposed to be from a genome. 

Beware of the defaults for `pH`, and concentrations of oligos, see if they make sense to you.

There is a `tests` folder, you could use some of these sequences for testing. For example, to test the predictions of the model agains the sequences that were used to test the model, and some that were added later for testing, you would do

```bash
tfo_finder -i for_validation.fna -val -rna
```

To analyse a chromosome,

```bash
tfo_finder -i hs_grch38.dna.chromosome.2.fna -ds  -b tfo_regions_in_genome.bed
```

where `tfo_regions_in_genome.bed` is the `bed` file with the locations of the *putative* regions where RNA could bind to form a triplex. The default output is `found_tfos.fa`, and contains the sequences,  with a header containing the location (and strand) in the genome, as well as thermodynamic prediction ( free energy followed by the melting temperature). *Please note that the Tm can be larger than 100C if the chain is very long, this is a limitation of the additive model we borrowed from Roberts&Crothers.*

For example, the header for each potential TFO (taken from the output of the execution above), could look like,

```
>2 dna:chromosome chromosome:GRCh38:2:1:242193529:1 REF|244495:244541|+|-33.57|55.95|Id=5
```

which means that the sequence listed below that was found in chromosome 2, in between the bases 244495 and 244541 in the positive sense, with a predicted free energy of formation of 33.57 kcal/mol, and a Tm of 55.95 C (at pH 7). The last id is just for reference counting.


### How to compile

First things first: this repo was not meant to be used to distribute the code, was just for me to keep track of it, so the "deployment" is probably not optimal.

Clone this repository, and move to the root directory. Then simply issue
```bash 
cmake . 
make
# or  make -j 4 to speed up

```
and you should get the compiled program.  You would need a modern version of CMake (3.5 or above), Seqan library (2.2 or above), ViennaRNA and Boost (yep...). Actually, I would be surprised if you could compile it without problems the first time around. 

If `cmake` can not find the dependencies by itself, either fix the `CMakeLists.txt` file or give it a little help. I include a few `sh` scripts with the details on the calling `cmake` with the right arguments for a few of my machines, over the years. This is the last one, which compiled in my MBP, 

```bash
cmake . -DCMAKE_CXX_COMPILER=/opt/local/bin/clang++-mp-7.0  -DCMAKE_C_COMPILER=/opt/local/bin/clang-mp-7.0
```
I've compiled it in a `linux` as well, but don't have the commands anymore.


##### Requirements

It's a pain to set up, I know. You would need:

1. Compiler supporting at least C++14 (required to compile Seqan anyway)
2. RNAfold  (ViennaRNA code)
3. Boost (regex)
2. CMake 3.5.0 or above
3. Seqan 2.2.0 (other versions untested, might work as well)


> **IMPORTANT**: *do* use -O3 for production, makes the codes way faster although the compiler takes a tad longer

### Bonus code

Check out [lncr_process_rnafold.py](lncr_process_rnafold.py). It was written long ago in python 2.7, but integrates RNAfold in the prediction. Much slower than the C++ counterpart.



##### Code organisation
The code is organised such that you have the command line parsing in one hpp file, the routines to compute the predicted stabilities of TFOs in another one, and main class that does the job. The class is called LncRna because that was the original intention, but the code grew to treat double stranded DNA (trivially). Each object of the lncrna class has one or severall Tfo objects, which store the TFO for each input sequence. The file `find_tfo.hpp`, where the code that does most of the work is, should be well documented. The class makes heavy usage of Tags to differentiate between function calls that do the same but speciallized (eg. folding vs non-folding).

>**NOTE**: the code works fine, but it could use some refactoring


