#include <seqan/seq_io.h>

using namespace seqan;

int main()
{
    CharString seqFileName = "example.fq";
    CharString id;
    CharString qual;
    Rna5String seq;

    SeqFileIn seqFileIn(toCString(seqFileName));
    readRecord(id, seq, qual, seqFileIn);
    std::cout << id << '\t' << seq << '\n';
    std::cout << qual << '\n';

    return 0;
}
