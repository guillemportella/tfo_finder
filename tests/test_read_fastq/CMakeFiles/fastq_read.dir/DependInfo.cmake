# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/Users/guillem/work/soft/genomics/Seqan_coding/tfo_finder/test_read_fastq/fastq_read.cpp" "/Users/guillem/work/soft/genomics/Seqan_coding/tfo_finder/test_read_fastq/CMakeFiles/fastq_read.dir/fastq_read.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "AppleClang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "SEQAN_HAS_EXECINFO=1"
  "SEQAN_HAS_ZLIB=1"
  "_FILE_OFFSET_BITS=64"
  "_LARGEFILE_SOURCE"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/opt/local/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
