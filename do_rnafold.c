#include "do_rnafold.h"

void do_rnafold(char const *seq, char const *constr, res_rnafold *fold) {
  // Mainly taken from the example of the API, removing stuff I don't need
  // I presume that you did check if the seq and constr are of the same length
  // I presume there is more checking going on inside the calls
  char *mfe_structure = vrna_alloc(sizeof(char) * (strlen(seq) + 1));
  vrna_fold_compound_t *vc =
      vrna_fold_compound(seq, NULL, VRNA_OPTION_MFE | VRNA_OPTION_PF);
  // Add constraints from a dot-bracket structure
  // Copied from RNAfold, seems to work
  unsigned int constraint_options = VRNA_CONSTRAINT_DB_DEFAULT;
  vrna_constraints_add(vc, (const char *)constr, constraint_options);
  /* call MFE function */
  double mfe = (double)vrna_mfe(vc, mfe_structure);
  strcpy(fold->structure, mfe_structure);
  fold->fe = mfe;
  free(mfe_structure);
  vrna_fold_compound_free(vc);
}

void do_rnafold_nc(char const *seq, res_rnafold *fold) {
  // Mainly taken from the example of the API, removing stuff I don't need
  // I presume there is more checking going on inside the calls
  char *mfe_structure = vrna_alloc(sizeof(char) * (strlen(seq) + 1));
  vrna_fold_compound_t *vc =
      vrna_fold_compound(seq, NULL, VRNA_OPTION_MFE | VRNA_OPTION_PF);
  /* call MFE function */
  double mfe = (double)vrna_mfe(vc, mfe_structure);
  strcpy(fold->structure, mfe_structure);
  fold->fe = mfe;
  free(mfe_structure);
  vrna_fold_compound_free(vc);
}
